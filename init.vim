"TODO: Add OSX specific settings!
set pyxversion=3
set encoding=utf-8
scriptencoding utf-8
filetype plugin indent on             " try to recognize filetypes
let mapleader = " "
runtime addons.vim
runtime cmdlets.vim
runtime editor.vim
runtime vimawesome.vim

if (has("termguicolors"))
  set termguicolors
endif

syntax on
colorscheme onedark

"Start screen
"
let g:startify_session_persistence = 0
let g:startify_change_to_vcs_root = 1
let g:startify_fortune_use_unicode = 0


let g:logo = [
            \'     ██╗███╗   ██╗███████╗ ██████╗ ██╗   ██╗██╗███╗   ███╗',
            \'     ██║████╗  ██║██╔════╝██╔═══██╗██║   ██║██║████╗ ████║',
            \'     ██║██╔██╗ ██║█████╗  ██║   ██║██║   ██║██║██╔████╔██║',
            \'██   ██║██║╚██╗██║██╔══╝  ██║   ██║╚██╗ ██╔╝██║██║╚██╔╝██║',
            \'╚█████╔╝██║ ╚████║███████╗╚██████╔╝ ╚████╔╝ ██║██║ ╚═╝ ██║',
            \' ╚════╝ ╚═╝  ╚═══╝╚══════╝ ╚═════╝   ╚═══╝  ╚═╝╚═╝     ╚═╝',
            \'----------------------------------------------------------',
            \'> Version: 1.2',
            \]

let g:startify_custom_header =
       \ 'map(g:logo + startify#fortune#cowsay(), "\"   \".v:val")'

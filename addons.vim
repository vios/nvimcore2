" Auto install plugins (at least try...)
if empty(glob('~/.local/share/nvim/plugged'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Plugin Load File
call plug#begin('~/.local/share/nvim/plugged')

Plug 'myusuf3/numbers.vim' " Better relative and absolute line number switching
Plug 'rust-lang/rust.vim'
Plug 'vim-airline/vim-airline' | Plug 'vim-airline/vim-airline-themes'
Plug 'neoclide/coc.nvim', {'do': { -> coc#util#install()}}
Plug 'rbgrouleff/bclose.vim'
Plug 'mhinz/vim-startify'
Plug 'rliang/termedit.nvim'
Plug 'mattn/emmet-vim', { 'for': 'html' }
Plug 'othree/javascript-libraries-syntax.vim', { 'for': 'javascript' }
Plug 'sirver/ultisnips' | Plug 'honza/vim-snippets' " Addon for ultisnips
Plug 'othree/yajs.vim', {'for': 'javascript'}
Plug 'nathanaelkane/vim-indent-guides'
Plug 'othree/html5.vim', { 'for': 'html' }
Plug 'SirJson/sd.vim'
Plug 'jiangmiao/auto-pairs'
Plug 'editorconfig/editorconfig-vim'
Plug 'tpope/vim-surround'
Plug 'mbbill/undotree'
Plug 'airblade/vim-gitgutter'
Plug 'ntpeters/vim-better-whitespace'
Plug 'sheerun/vim-polyglot' "More syntax highlighting info!
Plug 'SirJson/ranger.vim', { 'branch': 'change-directory' }
Plug 'kshenoy/vim-signature' " Tag addon
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' } | Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-fugitive'
Plug 'skywind3000/vim-quickui'

" Colorthemes!
Plug 'joshdick/onedark.vim'

call plug#end()

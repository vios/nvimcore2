" standard settings

function! EditorDefault() abort
    set cmdheight=2
    set noswapfile " For my usecase I better do not swap
    set signcolumn=yes
    set completeopt-=preview
    set tabstop=4
    set shiftwidth=4
    set softtabstop=4
    set expandtab
    set nowrap
    set list
    set listchars=tab:🠮🞗,space:∙,trail:~,nbsp:▒
    syntax on
    set number
    set relativenumber
    set ignorecase
    set smartcase
    set incsearch
    set lazyredraw
    set foldmethod=indent
    set foldnestmax=10
    set nofoldenable
    set foldlevel=2
    syntax on
    set completeopt=longest,menuone,preview
    set previewheight=5
    set autoindent
    set showmode
    set ttimeout
    set ttimeoutlen=0
endfunction


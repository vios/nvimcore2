# ViOS

## Dependencies

TBD

## Keybindings

- `CTRL` + `Space`: code completion
- `[c` + `]c`: navigate diagnostics
- `gd`: goto definition
- `gy`: goto type
- `gi`: goto implementation
- `g` + `r`: goto references
- `??`: expanding current directory
- `Space` + `cd`: auto cd to current directory
- `g` + `n`: go to next file
- `g` + `b`: go to prev file
- `Space` + `xx`: close file
- `Space` + `fn`: new file
- `F3`: Toggle undo tree
- `F7`: Fix indentation
- `CTRL` + `P`: Quick open
- `CTRL` + `F`: Ripgrep search
- `Space` + `l`: Lists
- `Space` + `p`: Actions
- `Space` + `b`: Buffers
- `ALT` + `T`: Toggle terminal

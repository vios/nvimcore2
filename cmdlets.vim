""""
" Helper for browsing internal docs
""""
function! AutoNetRwCwd()
    cd %:p:h
endfunction

let g:term_bot_win = -1
function! TermToggle(height) abort
    if g:term_bot_win > -1 && win_gotoid(g:term_bot_win)
        hide
    else
        bot new
        exec "resize " . a:height
        call termopen($SHELL, {"detach":0})
        let g:term_bot_buf = bufnr("")
        set nonumber
        set norelativenumber
        set signcolumn=no
        set noruler
        startinsert!
        let g:term_bot_win = win_getid()
    endif
endfunction

function! OpenConfig() abort
    let s:rootdir = expand("$VIMCONFIG")
    let s:file = s:rootdir . "/editor.vim"
    exec "tabedit " . s:file
    exec "lcd " . s:rootdir
endfunction

function! OpenPluginList() abort
    let s:rootdir = expand("$VIMCONFIG")
    let s:file = s:rootdir . "/addons.vim"
    exec "tabedit " . s:file
    exec "lcd " . s:rootdir
endfunction

function! SystemUpdate() abort
    source $VIMCONFIG/addons.vim
    :PlugClean
    :PlugUpgrade
    :PlugUpdate
endfunction

function! OpenTerminalWindow() abort
    tabnew
    terminal
endfunction

function! ManDB(manpage) abort
    exec "terminal man ".a:manpage
endfunction

command! GotoDir RangerCwd
command! Reload source $MYVIMRC
command! Config call OpenConfig()
command! Plugins call OpenPluginList()
command! Addons call OpenPluginList()
command! SysUpdate call SystemUpdate()
command! ClearMarks :delmarks!
command! NewTerm call OpenTerminalWindow()
command! Man call ManDB("<args>")
command! -bang -nargs=* Find
  \ call fzf#vim#grep(
  \   'rg --column --pretty --no-heading --color=always --smart-case '.shellescape(<q-args>), 1,
  \   fzf#vim#with_preview({'down': '70%','options': '--border --margin 4%,1% --reverse --prompt="Search in Files > "'}),
  \   <bang>0)

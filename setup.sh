#!/bin/bash

export HAS_ALLOW_UNSAFE=y

function has()
{
	curl -sL https://git.io/_has | bash -s $1
	return $?
}

if ! has curl
then
	echo "Install curl first!"
	exit 1
fi

if ! has git
then
	echo "Install git first!"
	exit 1
fi

if ! has python3
then
	echo "Python 3.x is needed!"
	exit 1
fi


if ! has pip
then
	echo "Pip is needed!"
	exit 1
fi


if ! has pynvim
then
	echo "pynvim not found"
	sudo pip install pynvim
	sudo pip3 install pynvim
fi


if ! has fzf
then
	echo "fzf not found. Installing fzf locally!"
	git clone --depth 1 https://github.com/junegunn/fzf.git $HOME/.fzf
	$HOME/.fzf/install
fi

if ! has node
then
	echo "nodejs not found. Installing nodejs via nvm..."
	curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.2/install.sh | bash
	export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
	[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
	nvm install stable
	npm install neovim
fi


echo "Installing Vim-Plug..."
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim


echo "Starting nvim for plugin install..."

nvim

echo "Done!"





















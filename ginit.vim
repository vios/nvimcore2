call rpcnotify(1, 'Gui', 'Font', 'Hack')
set termguicolors " truecolor!

let g:enable_bold_font = 1
let g:enable_italic_font = 0

if (has("termguicolors"))
  set termguicolors
endif
syntax on
colorscheme onedark
"let g:airline_theme='onedark'
runtime guiext.vim
